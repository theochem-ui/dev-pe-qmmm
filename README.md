# DEV-PE-QMMM

This is a development branch for the PE-QMMM. A new cpp based SCME source code is coupled with ASE+GPAW branches where the PE-QMMM routines are optimized. The cpp SCME code includes legacy H2O SCME, ACN SCME and flexible H2O SCME. The generalized code and routines allow for the coupling of any SCME model potential to the PE-QMMM routines in the ASE+GPAW.

## Installation Instructions

TBD


